import Head from 'next/head'
import Link from 'next/link'
import { Button, Card, Image, Header } from 'semantic-ui-react'
import { Product } from 'shopify-buy'
import { client } from '../utils/shopify'

const HomePage = ({ products }: any) => {
  console.log('products', products)
  return (
    <div>
      <Head>
        <title>Create Next App</title>
      </Head>
      <Card.Group itemsPerRow={3}>
        {products &&
          products.map((product: Product) => (
            <Link href={`/products/${product.id}`}>
              <Card>
                {product.images && <Image src={product.images[0].src} />}
                <Card.Content>
                  <Header as="h3">{product.title}</Header>
                  <span>{product.variants[0].formattedPrice}</span>
                </Card.Content>
              </Card>
            </Link>
          ))}
      </Card.Group>
      <Button color="purple">Purple</Button>
    </div>
  )
}

export async function getServerSideProps() {
  // Fetch data from external API
  const products: ShopifyBuy.Product[] = await client.product.fetchAll()

  // Pass data to the page via props
  return { props: { products: JSON.parse(JSON.stringify(products)) } }
}

export default HomePage
