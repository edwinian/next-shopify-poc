import { useRouter } from 'next/router'
import { client } from '../../utils/shopify'
import {
  Container,
  Header,
  Grid,
  Image,
  Menu,
  Sidebar,
  Visibility,
  Segment,
  List,
  Input,
  InputOnChangeData,
} from 'semantic-ui-react'
import { useState } from 'react'
const { Row, Column } = Grid

const Product = ({ product }: { product: ShopifyBuy.Product }) => {
  const router = useRouter()
  const { productId } = router.query
  const [focusImage, setFocusImage] = useState<ShopifyBuy.Image>(
    product.images[0]
  )
  const [quantity, setQuantity] = useState(0)
  const addToCart = async () => {
    let checkoutId = localStorage.getItem('checkoutId')
    if (!checkoutId) {
      const checkout: ShopifyBuy.Cart = await client.checkout.create()
      localStorage.setItem('checkoutId', `${checkout.id}`)
      checkoutId = `${checkout.id}`
    }
    const cart = await client.checkout.addLineItems(checkoutId, [
      {
        variantId: product.variants[0].id,
        quantity,
      },
    ])
    localStorage.setItem('cart', JSON.stringify(cart))
  }

  return (
    <Grid>
      <Row column={2}>
        <Column width={10}>
          <Row>
            <Image src={focusImage.src} />
          </Row>
          {product.images.length > 1 && (
            <Row>
              <List horizontal divided>
                {product.images.map((image, index) => (
                  <List.Item index={index} onClick={() => setFocusImage(image)}>
                    <Image size="small" src={image.src} />
                  </List.Item>
                ))}
              </List>
            </Row>
          )}
        </Column>
        <Column style={{ marginTop: 50 }} width={6}>
          <Input
            action={{
              color: 'teal',
              labelPosition: 'left',
              icon: 'cart',
              content: 'Checkout',
              onClick: addToCart,
            }}
            type="number"
            actionPosition="left"
            placeHolder="Search ..."
            defaultValue="52.03"
            onChange={(e, data: InputOnChangeData) =>
              setQuantity(Number(data.value))
            }
          />
        </Column>
      </Row>
    </Grid>
  )
}

export async function getServerSideProps({ query }: any) {
  // Fetch data from external API
  const product: ShopifyBuy.Product = await client.product.fetch(
    query.productId
  )

  // Pass data to the page via props
  return { props: { product: JSON.parse(JSON.stringify(product)) } }
}

export default Product
