import '../styles/globals.css'
import type { AppProps } from 'next/app'
import 'semantic-ui-css/semantic.min.css'
import {
  Container,
  Header,
  Grid,
  Image,
  Menu,
  Sidebar,
  Visibility,
  Segment,
  Button,
} from 'semantic-ui-react'
import { useState } from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'

const NavBar = () => {
  const router = useRouter()
  const [fixed, setFixed] = useState<boolean>()
  const renderHeader = [
    { name: 'Home', href: '/' },
    { name: 'About Us', href: '/about' },
  ]

  const onCheckout = () => {
    const cart = JSON.parse(localStorage.getItem('cart') || '')
    router.replace(cart.webUrl)
  }

  return (
    <Visibility
      once={false}
      onBottomPassed={() => setFixed(true)}
      onBottomVisibleReverse={() => setFixed(false)}
    >
      <Segment
        textAlign="center"
        style={{ minHeigh: 50, padding: '1em 2em', backgroundColor: 'black' }}
      >
        <Menu
          {...(fixed ? { fixed: 'top' } : {})}
          inverted={!fixed}
          pointing={!fixed}
          secondary={!fixed}
          size="large"
        >
          <Container>
            {renderHeader &&
              renderHeader.map((option, i) => (
                <Link href={option.href}>
                  <Menu.Item>{option.name}</Menu.Item>
                </Link>
              ))}
            <Menu.Item position="right">
              <Button onClick={onCheckout}>checkout</Button>
            </Menu.Item>
          </Container>
        </Menu>
      </Segment>
    </Visibility>
  )
}

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <NavBar />
      <Component {...pageProps} />
    </>
  )
}

export default MyApp
